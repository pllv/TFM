I hold a Professional Master's Degree in Education (Secondary Mathematics Teacher Training)

My **Master's Thesis** had a specific focus on the identification of giftedness:

## Detection and promotion of giftedness in mathematics by solving rich problems. Particular case analysis: Asperger.

### Abstract: 

The aim of this paper is to enlighten educators about the advantages of a thoughtfully selected collection of challenging mathematical exercises, aligned with the syllabus, to recognise and enhance mathematical skills. This approach eliminates the need for psychological aptitude examinations. A comparative analysis was conducted to assess the results of a challenging problem-based assessment and the academic performance of pupils in their standard lessons. The research was carried out with a group of 89 students from secondary schools. The outcomes suggest that the addition of intricate tasks has the potential to enhance course assessments and unveil outstanding abilities.

July 2018